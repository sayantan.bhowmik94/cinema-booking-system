import os

import logging
from logging import getLogger, Logger

from pkg_resources import resource_filename  # @UnresolvedImport

logging.basicConfig(level = logging.ERROR)

_LOG = getLogger()

    
__version__ = '1.0.0.0'
__description__ = 'Book Movie'

