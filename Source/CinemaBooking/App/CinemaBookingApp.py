'''
Created on 27-Dec-2018

@author: sayantan
'''
import os
import sys

from datetime import datetime

from cliff.app import App
from cliff.commandmanager import CommandManager
from cliff.interactive import InteractiveApp

from Bookings import BookingMgr
from CinemaBooking import __description__,__version__



class CinemaBookingApp(App):
    CONF_FILE_PATH = os.path.normpath('~/.CinemaBooking/CinemaBooking.conf')
    CONF_LOG_FILE = os.path.normpath('~/CinemaBooking')
    
    def __init__(self):
        
        super(CinemaBookingApp, self).__init__(
                    description=__description__,
                    version=__version__,
                    command_manager=CommandManager('CinemaBooking.commands'),
                    interactive_app_factory=InteractiveApp,
                    deferred_help=True                                                
                                               )
        
        
        self._ConfFile = os.path.abspath(os.path.expandvars(os.path.expanduser(self.CONF_FILE_PATH))) 
        fileBasePath = os.path.dirname(self._ConfFile)
        if not os.path.exists(fileBasePath):
            os.makedirs(fileBasePath)
    
        with open(self._ConfFile, 'a'):
            os.utime(self._ConfFile, None)
            
        self._BMgr = BookingMgr()
        
                   
    def initialize_app(self, argv):
        self._BMgr.load_conf_file(self._ConfFile)
     
 
     
    def EmployeeLogin(self, userName, password):
        self.LOG.info('Validating login information')
        self._BMgr.EmployeeLogin(userName, password)

    def EmployeeLogout(self):
        self.LOG.info('Logging Out of Server')
        self._BMgr.EmployeeLogout()
    
    def GetMovie(self):
        self.LOG.info('Movie Detail')
        return self._BMgr.GetMovie()
    
    def GetMovieDetail(self, movieName, movieDate, movieTime):
        self._BMgr.GetMovieDetail(movieName, movieDate, movieTime)    
    
    
    def GetMovieByDate(self, movieDate):
        params = self._BMgr.GetMovieByDate(movieDate)
        for key,val in params.iteritems():
            self.LOG.info('Movie Date %s' %key)
            self.LOG.info('===================')
            for _val in val:
                print _val
    
    def AddMovie(self, movieName, movieDesc, movieScreenTime, movieDate):
        self._BMgr.AddMovie(movieName, movieDesc, movieScreenTime, movieDate)
        self.LOG.info('Movie Has been added successfully !')
    
    def ConsumerLogin(self, userName, password):
        self.LOG.info('Validating login information')
        self._BMgr.ConsumerLogin(userName, password)

    def ConsumerLogout(self):
        self.LOG.info('Logging Out of Server')
        self._BMgr.ConsumerLogout()
        
    
    def BookMovie(self, userName, movieName, movieScreenTime, movieDate):
        self.LOG.info('Reserving Movie')
        self._BMgr.BookMovie(userName, movieName, movieScreenTime, movieDate)
        self.LOG.info('Reservation Confirmed.')
        print 'Movie Name :: %s' %movieName
        print 'Movie Time :: %s '%movieScreenTime
        print 'Movie Date :: %s' %movieDate
        print '=============================='
    
    def DelMovie(self,  userName, movieName, movieScreenTime, movieDate):
        self._BMgr.DelMovie(userName, movieName, movieScreenTime, movieDate)
       
    
    def GetBookingInfo(self,userName):
        stat = self._BMgr.GetBookingInfo(userName)
        if stat:
            self.LOG.warning('Unable to find and booking for the User')
       
    
    
    
def main():
    argv = sys.argv[1:]
    os.environ['REQUESTS_CA_BUNDLE'] = '/etc/ssl/certs/ca-certificates.crt'
    datetime.strptime('2012-01-01', '%Y-%m-%d')
    app = CinemaBookingApp()
    retcode = app.run(argv)
    return retcode
        
if __name__ == '__main__':
    retCode = main()
    exit(retCode)
