'''
Created on 28-Dec-2018

@author: sayantan
'''
from ConfigParser import ConfigParser
import datetime

class BookingMgr(object):

    def __init__(self):
        self._ConfFilePath = None
        
        self._Parser = ConfigParser()
        self._Parser.optionxform = str
      
    def load_conf_file(self, ConfFilePath):
        self._ConfFilePath = ConfFilePath
        self._Parser.read(self._ConfFilePath)
        
        self._EmpLoginSecName = 'EmployeeLoginStatus'
        if not self._Parser.has_section('EmployeeLoginStatus'):
            self._Parser.add_section(self._EmpLoginSecName)
            self.__WriteValues()
            
        self._ConsumerSecName = 'ConsumerLoginStatus'
        if not self._Parser.has_section('ConsumerLoginStatus'):
            self._Parser.add_section(self._ConsumerSecName)
            self.__WriteValues()
            
            
        self._TimeFormat = "%H:%M:%S"
        self._DateFormat = "%Y-%m-%d"
        
    def __IsLoggedIn(self, sectionName):
        status = False
        if self._Parser.has_section(sectionName):
            settings = self.__GetSettings(sectionName)
            if settings:
                if settings.get('Active') == 'True':
                    status = True
                
        return status
    
    def __WriteValues(self):
        with open(self._ConfFilePath, 'w') as f:
            self._Parser.write(f)
    
    def __GetSettings(self, sectionName):
        retDict = None
        
        if not self._Parser.has_section(sectionName):
            return dict()
        
        settingList = self._Parser.items(sectionName)
        if settingList:
            retDict = dict()
            for settingName, settingValue in settingList:
                retDict[settingName] = settingValue
                
        return retDict
    
    def __Logout(self, sectionName):
        if self.__IsLoggedIn(sectionName):
            self._Parser.set(sectionName, 'Active', 'False')
            self.__WriteValues()
            print 'Log-out Success-full !'
        else:
            print 'Not Logged-In'
    
    def __IsScreenTimeAvailable(self, screenTime, movieName):
        status = True
        screenTimeObj = datetime.datetime.strptime(screenTime,self._TimeFormat)
        movieParams = self.__GetSettings(movieName)
        
        _screenTime = movieParams.get('ScreenTime')
        if _screenTime:
            _scTime = [x.strip() for x in _screenTime.split(',')]
            
            if screenTime in _scTime:
                raise Exception ('Screen has been already added ! ')
            for _time in _scTime:
                dateObj = datetime.datetime.strptime(_time,self._TimeFormat)
                
                delta = screenTimeObj - dateObj
                if delta.days == -1:
                    delta = dateObj - screenTimeObj
                    
                if delta.seconds < 3600:
                    status = False
                
        return status
        
    def __UpdateScreenTime(self, screenTime, movieName):
        movieParams = self.__GetSettings(movieName)
        _screenTime = movieParams.get('ScreenTime')
        if _screenTime:
            _scTime = [x.strip() for x in _screenTime.split(',')]
            if screenTime not in _scTime:
                _scTime.append(screenTime)
        else:
            _scTime = [screenTime]
                   
        return ','.join(_scTime)

    def __UpdateMovieDate(self, movieDate, movieName):
        movieParams = self.__GetSettings(movieName)
        _date = movieParams.get('Date')
        if _date:
            _scTime = [x.strip() for x in _date.split(',')]
            if movieDate not in _date:
                _scTime.append(movieDate)
        else:
            _scTime = [movieDate]
                   
        return ','.join(_scTime)

    def __UpdateMovieInfo(self, movieName):
        movieParams = self.__GetSettings('Employee')
        _params = movieParams.get('Movies')
        if _params:
            _scTime = [x.strip() for x in _params.split(',')]
            if movieName not in _scTime:
                _scTime.append(movieName)
        else:
            _scTime = [movieName]
                   
        return ','.join(_scTime)


    
    def __GenerateMovieKey(self, movieName, movietime, movieDate):
        movieName = movieName.strip().replace(" ","_")
        key = "%s_%s_%s" %(movieName, movietime, movieDate)
        return key
    
    def __IsValidTime(self, movieTime):
        try:
            datetime.datetime.strptime(movieTime, self._TimeFormat)
            return True
        
        except ValueError:
            raise ValueError('Invalid Movie Time Format. Valid Time Format HH:MM:SS')

    
    def __IsValidDate(self, movieDate):
        try:
            datetime.datetime.strptime(movieDate, self._DateFormat)
            return True
        
        except ValueError:
            raise ValueError('Invalid Movie Date Format. Valid Date Format YYYY-MM-DD')

    
    def __Login(self, userName, password, sectionName, loginType):
        self._Parser.read(self._ConfFilePath)
        
        if self._Parser.has_section(loginType):
            settings = self.__GetSettings(loginType)
            if not settings.get(userName):
                raise Exception('Unable to find any Credentials for User %s' % userName)
            if settings.get(userName) == password:
                print 'Login Success-full'
                self._Parser.set(sectionName, 'Active', 'True')
                self.__WriteValues()
           
            else:
                raise Exception('Invalid Password')
        else:
            raise Exception('Unable to find any %s Credentials' % loginType)
        
        return True
    
    def EmployeeLogin(self, userName, password):
        self.__Login(userName, password, self._EmpLoginSecName, 'Employee')
        return True

    def EmployeeLogout(self):
        self.__Logout(self._EmpLoginSecName)
        
        
    def AddMovie(self, movieName, movieDesc, movieScreenTime, movieDate):
        if not self.__IsLoggedIn(self._EmpLoginSecName):
            raise Exception('Please Login into the server !')
        else:
            self.__IsValidDate(movieDate)
            self.__IsValidTime(movieScreenTime)
            if not self.__IsScreenTimeAvailable(movieScreenTime, movieName):
                raise Exception('Screen Time Not Available. Please select different screen time !')
            if not self._Parser.has_section(movieName):
                self._Parser.add_section(movieName)
        
            self._Parser.set(movieName, 'Description', movieDesc)
            self._Parser.set(movieName, 'ScreenTime', self.__UpdateScreenTime(movieScreenTime, movieName))
            self._Parser.set(movieName, 'Date', self.__UpdateMovieDate(movieDate, movieName))
            self._Parser.set('Employee','Movies', self.__UpdateMovieInfo(movieName))
            
            movieKey = self.__GenerateMovieKey(movieName, movieScreenTime, movieDate)
                
            if not self._Parser.has_section(movieKey):
                self._Parser.add_section(movieKey)
               
            self._Parser.set(movieKey, 'Available', 100)
            self._Parser.set(movieKey, 'Reserved', 0)
            self._Parser.set(movieKey, 'ScreenTime', movieScreenTime)
            self._Parser.set(movieKey, 'MovieName', movieName)
            
            self.__WriteValues()
            
        return True
    
    
    def  GetMovie(self):
        emp = self.__GetSettings('Employee')
        movieDetails = emp.get('Movies')
        if not movieDetails:
            print 'No Movie Details Found in record !!'
            
        else:
            movieDetails = [x.strip() for x in movieDetails.split(',')]
            movieParms = dict()
            for movie in movieDetails:
                info = self.__GetSettings(movie)
                movieParms[movie] = info
        
        return movieParms
        
    def GetMovieDetail(self, movieName, movieDate, movieTime):
        self.__IsValidDate(movieDate)
        self.__IsValidTime(movieTime)
        
        key = self.__GenerateMovieKey(movieName, movieTime, movieDate)
        params = self.__GetSettings(key)
        
        if params:
            print 'Available Ticket %s ' % params.get('Available')
            print 'Reserved Ticket %s ' % params.get('Reserved')
        else:
            print 'Unable to find any movie details for given params !'
    
    def GetMovieByDate(self, movieDate):
        self.__IsValidDate(movieDate)
        params = dict()
        params[movieDate] = list()
        sections = self._Parser.sections()
        for section in sections:
            if movieDate in section:
                val = self.__GetSettings(section)
                movieName = val.get('MovieName')
                movieST = val.get('ScreenTime')
                movieAvaiable = val.get('Available')
                movieBooked = val.get('Reserved')
                
                params[movieDate].append({'movieName':movieName,'screenTime':movieST,'available':movieAvaiable,'reserved':movieBooked})
        
        
      
        return params
    
    def ConsumerLogin(self, userName, password):
        self.__Login(userName, password, self._ConsumerSecName, 'Consumer')
        if not self._Parser.has_section(userName):
            self._Parser.add_section(userName)
            self.__WriteValues()
            
        return True

    def ConsumerLogout(self):
        self.__Logout(self._ConsumerSecName)
        
    def __UpdateCustomerInfo(self, movieName, movieDate, movieTime, reserved, userName):
        movieName = movieName.strip().replace(" ","_")
        key = "%s_%s_%s_%s" %(userName, movieName, movieTime, movieDate)
        if not self._Parser.has_section(key):
            self._Parser.add_section(key)
    
        self._Parser.set(key, 'Name', movieName)
        self._Parser.set(key, 'Time', movieTime)
        self._Parser.set(key, 'Date', movieDate)
        self._Parser.set(key, 'Reserved', reserved)
        
        self.__WriteValues()
    
                
    
    def GetBookingInfo(self, userName):
        stat = True
        if not self.__IsLoggedIn(self._ConsumerSecName):
            raise Exception('Please Login into the server !')
        else:
            sections = self._Parser.sections()
            for sec in sections:
                if userName in sec:
                    _keyName = sec
                    params = self.__GetSettings(_keyName)
                    if params:
                        stat = False
                        print '============================'
                        print 'Movie Name :: %s' %params.get('Name')
                        print 'Movie Time :: %s '%params.get('Time')
                        print 'Movie Date :: %s' %params.get('Date')
                        print 'Movie Reservation :: %s' %params.get('Reserved')
                        print '=============================='
                    
                
                    
        return stat
    
    def DelMovie(self, userName, movieName, movieTime, movieDate):
        if not self.__IsLoggedIn(self._ConsumerSecName):
            raise Exception('Please Login into the server !')
        
        self.__IsValidDate(movieDate)
        self.__IsValidTime(movieTime)
        movieName = movieName.strip().replace(" ","_")
        key = "%s_%s_%s_%s" %(userName, movieName, movieTime, movieDate)
        
        if self._Parser.has_section(key):
            self._Parser.remove_section(key)
            self.__WriteValues()
            print 'Booking has been removed !!'
        else:
            print 'Unable to find any matching bookings'
       
    
    
    def BookMovie(self, userName, movieName, movieTime, movieDate):
        if not self.__IsLoggedIn(self._ConsumerSecName):
            raise Exception('Please Login into the server !')
        else:
            self.__IsValidDate(movieDate)
            self.__IsValidTime(movieTime)
            movieParams = self.GetMovie()
            if movieParams:
                movieKey = None
                _val = movieParams.get(movieName)
                
                if not _val:
                    raise Exception ('Movie %s is not available' %movieName)
                else:
                    #_date = _val.get('Date')
                    #_screenTime = _val.get('ScreenTime')
                    _date = [x.strip() for x in _val.get('Date').split(',')]
                    _screenTime = [x.strip() for x in _val.get('ScreenTime').split(',')]
                    
                    if (
                        movieTime in _screenTime 
                        and
                        movieDate in _date
                        ):
                        movieKey = self.__GenerateMovieKey(movieName, movieTime, movieDate) 
                    else:
                        raise Exception ('Unable to find matching screen time or name')
                
                movieSettings = self.__GetSettings(movieKey)
                available = int(movieSettings.get('Available'))
                reserved = int(movieSettings.get('Reserved'))
                
                if available > 0  :
                    available = available - 1
                    reserved = reserved + 1
                    
                    self._Parser.set(movieKey, 'Available', str(available))
                    self._Parser.set(movieKey, 'Reserved', str(reserved))
                    
                    self.__UpdateCustomerInfo(movieName, movieDate, movieTime, reserved, userName)
                    
                    self.__WriteValues()
                      
                else:
                    raise Exception('All Tickets are reserved !')
                
                
            else:
                raise Exception('Unable to find and movie on given date')
        
        
    
    
