
import logging
from cliff.lister import Lister


from cliff.command import Command

class Employee(Command):
    'Employee Commands'

    LOG = logging.getLogger(__name__)
    
    def get_parser(self, prog_name):
        parser = super(Employee, self).get_parser(prog_name)
        
        subparsers = parser.add_subparsers(
                                           title='Employee Server Commands',
                                           description='Commands to deal with employee login',
                                           help='Employee Server Commands',
                                           dest='EmployeeServerCommand',
                                           metavar='{EmployeeServerCommand}'
                                           
                                          )
        
        getdetails_subparser = subparsers.add_parser('getmoviedetails', help='Get Status of all movies')
        
        getdetails_subparser.add_argument(
                            '-n',
                            '--name',
                            action='store',
                            help='Movie Name',
                            required=True,
                            dest='GetMovieName'     
                           )
        
        getdetails_subparser.add_argument(
                            '-d',
                            '--date',
                            action='store',
                            help='Movie Date',
                            required=True,
                            dest='GetMovieDate'     
                           ) 
        
        getdetails_subparser.add_argument(
                            '-t',
                            '--time',
                            action='store',
                            help='Movie Time',
                            required=True,
                            dest='GetMovieTime'     
                           ) 
        
        
        
        addMovie_subparser = subparsers.add_parser('addmovie', help='Add new movie')
        addMovie_subparser.add_argument(
                            '-n',
                            '--name',
                            action='store',
                            help='Movie Name',
                            required=True,
                            dest='MovieName'     
                           )
        
        addMovie_subparser.add_argument(
                            '-d',
                            '--desc',
                            action='store',
                            help='Movie Description',
                            required=True,
                            dest='MovieDesc'     
                           ) 
        
        addMovie_subparser.add_argument(
                            '-t',
                            '--time',
                            action='store',
                            help='Movie Screen Time',
                            required=True,
                            dest='MovieST'     
                           )  
        
        
        addMovie_subparser.add_argument(
                            '-r',
                            '--date',
                            action='store',
                            help='Movie Date',
                            required=True,
                            dest='MovieDate'     
                           )      
        
        login_subparser = subparsers.add_parser('login', help='User Login')
        login_subparser.add_argument(
                            '-u',
                            '--username',
                            action='store',
                            help='User Name',
                            required=True,
                            dest='UserName'     
                           )
        
        login_subparser.add_argument(
                            '-p',
                            '--pass',
                            action='store',
                            help='User Password',
                            required=True,
                            dest='Pass'     
                           )
        
        
        subparsers.add_parser('logout', help='Logout of the service')
        
       

        return parser
    
    def take_action(self, parsed_args):
        if parsed_args.EmployeeServerCommand == 'login':
            self.app.EmployeeLogin(parsed_args.UserName,parsed_args.Pass)
            return True
        
        if parsed_args.EmployeeServerCommand == 'logout':
            self.app.EmployeeLogout()
            return True
        
        if parsed_args.EmployeeServerCommand == 'getmoviedetails':
            self.app.GetMovieDetail(parsed_args.GetMovieName,parsed_args.GetMovieDate,parsed_args.GetMovieTime)
            return True
        
            
        if parsed_args.EmployeeServerCommand == 'addmovie':
            self.app.AddMovie(parsed_args.MovieName,parsed_args.MovieDesc, parsed_args.MovieST ,parsed_args.MovieDate)
            return True

'''
Created on 27-Dec-2018

@author: sayantan(sayantan.bhowmik94@gmail.com)
'''                    
      