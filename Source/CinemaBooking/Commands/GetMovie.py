'''
Created on 28-Dec-2018

@author: sayantan
'''
from cliff.lister import Lister

class GetMovie(Lister):
    'Get all the LocalExecutions'

    

    def take_action(self, parsed_args):
        movieDetails = self.app.GetMovie()
           
   
        formattedmyLocalExecution = list()

        for key, value in movieDetails.iteritems():
            formattedmyLocalExecution.append((key, value['Date'], value['Description'], value['ScreenTime']))

        return(
            ('Movie Name', 'Movie Date','Movie Desc','Movie Screen Time'),
            formattedmyLocalExecution
            )




