'''
Created on 27-Dec-2018

@author: sayantan
'''

import logging


from cliff.command import Command

class Consumer(Command):
    'Consumer Commands'

    LOG = logging.getLogger(__name__)
    
    def get_parser(self, prog_name):
        parser = super(Consumer, self).get_parser(prog_name)
        
        subparsers = parser.add_subparsers(
                                           title='Consumer Server Commands',
                                           description='Commands to deal with employee login',
                                           help='Consumer Server Commands',
                                           dest='ConsumerServerCommand',
                                           metavar='{ConsumerServerCommand}'
                                           
                                          )
        
        #subparsers.add_parser('getmovie', help='Get Status of all movies')
        bookMovie_subparser = subparsers.add_parser('bookinfo', help='Book Info')
        
        bookMovie_subparser.add_argument(
                            '-u',
                            '--username',
                            action='store',
                            help='UserName',
                            required=True,
                            dest='BookUserName'     
                           )
        
      
        
        delMovie_subparser = subparsers.add_parser('bookdel', help='Delete a reservation')
        
        delMovie_subparser.add_argument(
                            '-u',
                            '--username',
                            action='store',
                            help='UserName',
                            required=True,
                            dest='DUserName'     
                           )
        
        
        delMovie_subparser.add_argument(
                            '-n',
                            '--name',
                            action='store',
                            help='Movie Name',
                            required=True,
                            dest='DMovieName'     
                           )
        
              
        delMovie_subparser.add_argument(
                            '-t',
                            '--time',
                            action='store',
                            help='Movie Screen Time',
                            required=True,
                            dest='DMovieST'     
                           )  
        
        
        delMovie_subparser.add_argument(
                            '-r',
                            '--date',
                            action='store',
                            help='Movie Date',
                            required=True,
                            dest='DMovieDate'     
                           )   
        
        
        addMovie_subparser = subparsers.add_parser('book', help='Reserve a movie')
        
        addMovie_subparser.add_argument(
                            '-u',
                            '--username',
                            action='store',
                            help='UserName',
                            required=True,
                            dest='BUserName'     
                           )
        
        
        addMovie_subparser.add_argument(
                            '-n',
                            '--name',
                            action='store',
                            help='Movie Name',
                            required=True,
                            dest='MovieName'     
                           )
        
              
        addMovie_subparser.add_argument(
                            '-t',
                            '--time',
                            action='store',
                            help='Movie Screen Time',
                            required=True,
                            dest='MovieST'     
                           )  
        
        
        addMovie_subparser.add_argument(
                            '-r',
                            '--date',
                            action='store',
                            help='Movie Date',
                            required=True,
                            dest='MovieDate'     
                           )      
        
        
        getdMovie_subparser = subparsers.add_parser('getmoviebydate', help='Get Movie Info ')
        getdMovie_subparser.add_argument(
                            '-d',
                            '--date',
                            action='store',
                            help='Movie Date',
                            required=True,
                            dest='GetMovieDate'     
                           )
        
        
        
        login_subparser = subparsers.add_parser('login', help='User Login')
        login_subparser.add_argument(
                            '-u',
                            '--username',
                            action='store',
                            help='User Name',
                            required=True,
                            dest='UserName'     
                           )
        
        login_subparser.add_argument(
                            '-p',
                            '--pass',
                            action='store',
                            help='User Password',
                            required=True,
                            dest='Pass'     
                           )
        
        
        subparsers.add_parser('logout', help='Logout of the service')
        
       

        return parser
    
    def take_action(self, parsed_args):
        if parsed_args.ConsumerServerCommand == 'login':
            self.app.ConsumerLogin(parsed_args.UserName,parsed_args.Pass)
            return True
        
        if parsed_args.ConsumerServerCommand == 'logout':
            self.app.ConsumerLogout()
            return True
        
        if parsed_args.ConsumerServerCommand == 'getmoviebydate':
            self.app.GetMovieByDate(parsed_args.GetMovieDate)
            return True
        
        if parsed_args.ConsumerServerCommand == 'bookinfo':
            self.app.GetBookingInfo(parsed_args.BookUserName)
            return True
        
        if parsed_args.ConsumerServerCommand == 'book':
            self.app.BookMovie(parsed_args.BUserName, parsed_args.MovieName, parsed_args.MovieST ,parsed_args.MovieDate)
            return True
        
        if parsed_args.ConsumerServerCommand == 'bookdel':
            self.app.DelMovie(parsed_args.DUserName, parsed_args.DMovieName, parsed_args.DMovieST ,parsed_args.DMovieDate)
            return True


'''
Created on 27-Dec-2018

@author: sayantan(sayantan.bhowmik94@gmail.com)
'''                    
      