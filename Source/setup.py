#!/usr/bin/env python
import os


from setuptools import setup, find_packages
import CinemaBooking

try:
    long_description = open('README.rst', 'rt').read()
except IOError:
    long_description = ''
    
def load_pkg_dependencies():
    pkgDeps = list()
    
    if os.path.exists('requirements.txt'):
        reqsList = None
        with open('requirements.txt') as fp:
            reqsList = fp.readlines()
            
        if reqsList:
            for req in reqsList:
                reqSpec = req.strip()
                if reqSpec:
                    pkgDeps.append(reqSpec)
                    
    return pkgDeps  
    
pkg_dependencies = load_pkg_dependencies()
pkg_scripts = []
pkg_datalist = ['Conf/*.conf', 'version.txt']

setup(
    name=CinemaBooking.__name__,
    version=CinemaBooking.__version__,

    description=CinemaBooking.__description__,
    long_description=long_description,

    author='Sayantan',
    author_email='sayantan.bhowmik94@gmail.com',

    classifiers=[
                 'Development Status :: Alpha',
                 'License :: Proprietary :: SB Proprietary',
                 'Intended Audience :: Developers',
                 'Environment :: SB',
                ],

    platforms=['Any'],
    scripts=pkg_scripts,
    provides=[],
    install_requires=pkg_dependencies,
    namespace_packages=[],
    packages=find_packages(),
    include_package_data=True,
    package_data={'CinemaBooking' : pkg_datalist},

     entry_points={
        'console_scripts': [
            'booking = CinemaBooking.App.CinemaBookingApp:main'
        ],
        'CinemaBooking.commands': [
          
            'employee = CinemaBooking.Commands.Employee:Employee' ,
            'consumer = CinemaBooking.Commands.Consumer:Consumer' ,
            'getmovie = CinemaBooking.Commands.GetMovie:GetMovie'
                ],
    },
    
   

    zip_safe=False,
)
